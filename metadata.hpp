#pragma once

#include <vector>
#include <cstring>
#include <pthread.h>

#include "types.hpp"

struct metadata_t {
  int concurrency = 0;
  std::vector <pthread_mutex_t *> mutexes;
  std::vector <pthread_cond_t *> conds;

  bool check_mutex(pthread_mutex_t *mutex) {
    if (std::find(mutexes.begin(), mutexes.end(), mutex) != mutexes.end()) {
      return true;
    } else {
      if (memcmp(&default_mutex, mutex, sizeof(pthread_mutex_t)) == 0) {
        pthread_mutex_init(mutex, NULL);
        return true;
      } else {
        fprintf(stderr, "error: uninitialized mutex!\n");
        return false;
      }
    }
  }

  bool check_cond(pthread_cond_t *cond) {
    if (std::find(conds.begin(), conds.end(), cond) != conds.end()) {
      return true;
    } else {
      if (memcmp(&default_cond, cond, sizeof(pthread_cond_t)) == 0) {
        pthread_cond_init(cond, NULL);
      } else {
        fprintf(stderr, "error: uninitialized condition variable!\n");
        return false;
      }
    }
  }
};

struct call_thread_args {
  void *(*funcp) (void *);
  void *args;

  call_thread_args(void *(*funcp) (void *), void *args) :
    funcp(funcp), args(args) {}
};
