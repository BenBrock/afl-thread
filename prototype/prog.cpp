#include <cstdlib>
#include <cstdio>
#include <ucontext.h>
#include <unistd.h>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <set>
#include <memory>
#include <signal.h>
#include <sys/mman.h>
#include <fstream>

struct Thread {
  int tid;
  bool suspended = false;
  bool dead = false;
  ucontext_t context;

  Thread(const int tid) : tid(tid) {}
};

struct ThreadRuntime {
  const int STACK_SIZE = 8 * 1024*1024;
  int threadNum = 0;
  int cur_tid = 0;
  int dec_idx = 0;
  std::vector <unsigned int> decisions;
  std::unordered_map <int, std::shared_ptr <Thread>> threads;
  std::set <int> live_tids;

  ThreadRuntime(const std::vector <unsigned int> &decisions)
    : decisions(decisions) {
    cur_tid = threadNum++;
    threads[cur_tid] = std::shared_ptr <Thread> (new Thread(cur_tid));
    live_tids.insert(cur_tid);
  }

  int get_tid() {
    return cur_tid;
  }

  void join(const int tid) {
    while (!threads[tid]->dead) {
      yield();
    }
    threads.erase(threads.find(tid));
    live_tids.erase(tid);
  }

  void im_dead() {
    threads[cur_tid]->dead = true;
    live_tids.erase(cur_tid);
    schedule();
  }

  int launch(void (*funcp) (void *), void *args) {
    ucontext_t child_context;
    getcontext(&child_context);
    child_context.uc_link = 0;
    child_context.uc_stack.ss_sp = mmap(0, STACK_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, 0, 0);
    child_context.uc_stack.ss_size = STACK_SIZE;
    child_context.uc_stack.ss_flags = 0;
    if (child_context.uc_stack.ss_sp == 0) {
      printf("Error! Could not launch child!\n");
      return -1;
    }
    makecontext(&child_context, reinterpret_cast <void (*)()> (funcp), 1, args);

    // Okay, created child context.
    // Now store it.
    int child_tid = threadNum++;
    threads[child_tid] = std::shared_ptr <Thread> (new Thread(child_tid));
    threads[child_tid]->context = child_context;
    live_tids.insert(child_tid);

    yield();

    return child_tid;
  }

  void yield() {
    schedule();
  }

  void schedule() {
    if (threads.size() == 0) {
      return;
    } else {
      int index;
      if (dec_idx < decisions.size()) {
        index = decisions[dec_idx++];
      } else {
        index = lrand48();
      }
      index %= live_tids.size();
      int tid = *std::next(live_tids.begin(), index);

      if (tid != cur_tid) {
        int old_tid = cur_tid;
        cur_tid = tid;
        swapcontext(&threads[old_tid]->context, &threads[cur_tid]->context);
        cur_tid = old_tid;
      }
    }
  }
};

std::shared_ptr <ThreadRuntime> runtime;

struct args {
  int id;
  int *memory;
  args(const int id, int *memory)
    : id(id), memory(memory) {}
};

void early_access(void *v) {
  printf("I'm thread %d, I always access my memory ASAP!\n", runtime->get_tid());
  args *b = (args *) v;
  for (int i = 0; i < 100000; i++) {
    b->memory[i] = runtime->get_tid();
  }
  runtime->yield();
  printf("Wasn't that easy and safe?\n", runtime->get_tid());
  runtime->im_dead();
}

std::vector <unsigned int> get_decisions() {
  std::vector <unsigned int> decisions;
  std::shared_ptr <unsigned int> hey(new unsigned int[512]);
  size_t num_decisions = fread(hey.get(), sizeof(unsigned int), 512, stdin);
  for (int i = 0; i < num_decisions; i++) {
    decisions.push_back(hey.get()[i]);
  }
  return decisions;
}

int main(int argc, char **argv) {
  auto decisions = get_decisions();
  printf("Got %d decisions.\n", decisions.size());

  runtime = std::shared_ptr <ThreadRuntime> (new ThreadRuntime(decisions));

  runtime->yield();
  int *muh_mem = new int[100000];
  args a(1337, muh_mem);
  printf("Woohooo thread %d launching...\n", runtime->get_tid());
  int child_tid = runtime->launch(early_access, (void *) &a);
  runtime->yield();
  printf("Thread %d deleting!\n", runtime->get_tid());
  delete [] muh_mem;

  runtime->join(child_tid);

  return 0;
}
