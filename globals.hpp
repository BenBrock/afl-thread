#pragma once

const int MAX_DECISIONS = 8192;
const int STACK_SIZE = 8*1024*1024;

// Levels are 0 : quiet, 1 : debug, 2 : verbose
const int LOG_LEVEL = 2;
