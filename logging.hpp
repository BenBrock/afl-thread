#pragma once

#include "globals.hpp"
#include <string>

void log(const int log_level, const char *format, ...) {
  if (LOG_LEVEL >= log_level) {
    va_list argptr;
    va_start(argptr, format);
    vfprintf(stderr, format, argptr);
    va_end(argptr);
    fflush(stdout);
  }
}
