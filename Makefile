CXX = afl-clang-fast++

CPPFLAGS = -flto -c -std=gnu++14 -O3 -fPIC

all: pthread.o

pthread.o : pthread.cpp
	$(CXX) $(CPPFLAGS) pthread.cpp -o pthread.o

clean:
	@rm -fv *.o
