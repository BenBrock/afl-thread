#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <ucontext.h>
#include <cassert>
#include <set>
#include <vector>
#include <algorithm>
#include <memory>
#include <cstring>
#include <unordered_map>
#include <semaphore.h>

#include "runtime_globals.hpp"
#include "ThreadRuntime.hpp"
#include "metadata.hpp"
#include "types.hpp"
#include "logging.hpp"

extern "C" {
  #include <pthread.h>
}

std::vector <unsigned int> get_decisions () {
  std::vector <unsigned int> decisions;
  std::shared_ptr <unsigned int> hey(new unsigned int[8192]);
  size_t num_decisions = fread(hey.get(), sizeof(unsigned int), 8192, stdin);
  for (int i = 0; i < num_decisions; i++) {
    decisions.push_back(hey.get()[i]);
  }
  return decisions;
}

void mock_pthread_init() {
  if (!mock_pthread_initialized) {
    log(0, "Running the mock pthreads framework!\n");
    // Pass decisions here
    mock_pthread_initialized = true;
    runtime = new(ThreadRuntime);
    // runtime->decisions = get_decisions();
  }
}

void pthread_destroy() {
  if (mock_pthread_initialized) {
    delete runtime;
    mock_pthread_initialized = false;
  }
}

int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
  void *(*start_routine) (void *), void *arg) {
  mock_pthread_init();
  *thread = runtime->launch(start_routine, arg);

  if (attr != NULL) {
    log(2, "Warning: created pthread %d with non-null attr\n", *thread);
  }

  return 0;
}

int pthread_join(pthread_t thread, void **value_ptr) {
  mock_pthread_init();
  if (!runtime->thread_exists(thread)) {
    return 1;
  } else {
    if (value_ptr != NULL) {
      *value_ptr = runtime->join(thread);
    }
    return 0;
  }
}

void pthread_exit(void *retval) {
  mock_pthread_init();
  runtime->im_dead(retval);
}

pthread_t pthread_self() {
  pthread_t self;
  self = runtime->get_tid();
  return self;
}

int pthread_detach(pthread_t thread) {
  mock_pthread_init();
  if (!runtime->thread_exists(thread)) {
    return 1;
  } else {
    runtime->detach(thread);
    return 0;
  }
}

int pthread_mutex_init(pthread_mutex_t *__restrict__ mutex,
  const pthread_mutexattr_t *__restrict__ attr) {
  mock_pthread_init();
  runtime->metadata.mutexes.push_back((pthread_mutex_t *) mutex);
  semaphore *semi = (semaphore *) mutex;
  semi->lock = -1;
  if (attr != NULL) {
    if (*((int *) attr) == PTHREAD_MUTEX_NORMAL) {
      semi->type = PTHREAD_MUTEX_NORMAL;
    } else if (*((int *) attr) == PTHREAD_MUTEX_DEFAULT) {
      semi->type = PTHREAD_MUTEX_DEFAULT;
    } else if (*((int *) attr) == PTHREAD_MUTEX_RECURSIVE) {
      semi->type = PTHREAD_MUTEX_RECURSIVE;
    } else {
      log(1, "Warning: unsupported mutex attr requested by %d.\n", runtime->get_tid());
    }
  }
  return 0;
}

int pthread_mutex_lock(pthread_mutex_t *mutex) {
  mock_pthread_init();
  runtime->metadata.check_mutex(mutex);
  semaphore *semi = (semaphore *) mutex;
  if (semi->type != PTHREAD_MUTEX_RECURSIVE) {
    while (semi->lock != -1) {
      runtime->yield();
    }
  } else {
    while (semi->lock != -1 && semi->lock != runtime->get_tid()) {
      runtime->yield();
    }
  }
  semi->lock = runtime->get_tid();
  return 0;
}

int pthread_mutex_trylock(pthread_mutex_t *mutex) {
  mock_pthread_init();
  runtime->metadata.check_mutex(mutex);
  semaphore *semi = (semaphore *) mutex;
  if (semi->type == PTHREAD_MUTEX_RECURSIVE && semi->lock == runtime->get_tid()) {
    return 0;
  }
  if (semi->lock == -1) {
    semi->lock = runtime->get_tid();
    return 0;
  } else {
    return 1;
  }
}

int pthread_mutex_unlock(pthread_mutex_t *mutex) {
  mock_pthread_init();
  runtime->metadata.check_mutex(mutex);
  semaphore *semi = (semaphore *) mutex;
  if (semi->lock == runtime->get_tid()) {
    semi->lock = -1;
    return 0;
  } else {
    return 1;
  }
}

int sem_init(sem_t *sem, int pshared, unsigned int value) {
  mock_pthread_init();
  log(1, "Initializing semaphore... This is an untested code path.\n");
  semaphore *semi = (semaphore *) sem;
  semi->lock = value;
  return 0;
}

int sem_wait(sem_t *sem) {
  mock_pthread_init();
  semaphore *semi = (semaphore *) sem;
  while (semi->lock <= 0) {
    runtime->yield();
  }
  semi->lock--;
  return 0;
}

int sem_trywait(sem_t *sem) {
  mock_pthread_init();
  semaphore *semi = (semaphore *) sem;
  if (semi->lock > 0) {
    semi->lock--;
    return 0;
  } else {
    return 1;
  }
}

int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout) {
  mock_pthread_init();
  return sem_wait(sem);
}

int pthread_cond_init(pthread_cond_t *__restrict__ cond,
  const pthread_condattr_t *__restrict__ attr) {
  mock_pthread_init();
  runtime->metadata.conds.push_back((pthread_cond_t *) cond);
  cond_t *c = (cond_t *) cond;
  c->signaled = false;
  if (attr != NULL) {
    log(1, "Warning: tid %d initializing cond %p with non-null attr\n",
      runtime->get_tid(), cond);
  }
  return 0;
}

int pthread_cond_destroy(pthread_cond_t *cond) {
  mock_pthread_init();
  cond_t *c = (cond_t *) cond;
  c->signaled = false;
  return 0;
}

int pthread_cond_wait(pthread_cond_t * __restrict__ cond,
  pthread_mutex_t *__restrict__ mutex) {
  mock_pthread_init();
  runtime->metadata.check_cond(cond);
  cond_t *c = (cond_t *) cond;
  semaphore *semi = (semaphore *) mutex;
  // TODO: support case where pthread_cond_wait is called incorrectly?
  if (semi->lock != runtime->get_tid()) {
    fprintf(stderr, "error: called pthread_cond_wait with unheld mutex.\n");
    return 1;
  }
  if (runtime->threads[runtime->get_tid()]->cond_waiting()) {
    fprintf(stderr, "error: called pthread_cond_wait when thread is already waiting.\n");
  }
  runtime->threads[runtime->get_tid()]->cond_wait(c);
  while (runtime->threads[runtime->get_tid()]->cond_waiting()) {
    pthread_mutex_unlock(mutex);
    runtime->yield();
    pthread_mutex_lock(mutex);
  }
  return 0;
}

int pthread_cond_signal(pthread_cond_t *cond) {
  mock_pthread_init();
  cond_t *c = (cond_t *) cond;
  runtime->metadata.check_cond(cond);
  // A scheduling decision should go here.
  for (auto &thread_pair : runtime->threads) {
    auto &thread = thread_pair.second;
    if (thread->cond == c) {
      thread->cond = NULL;
      break;
    }
  }
  return 0;
}

int pthread_cond_broadcast(pthread_cond_t *cond) {
  mock_pthread_init();
  cond_t *c = (cond_t *) cond;
  runtime->metadata.check_cond(cond);
  for (auto &thread_pair : runtime->threads) {
    auto &thread = thread_pair.second;
    if (thread->cond == c) {
      thread->cond = NULL;
    }
  }
  return 0;
}

int pthread_setconcurrency(int new_level) {
  mock_pthread_init();
  runtime->metadata.concurrency = new_level;
  return 0;
}

int pthread_getconcurrency() {
  mock_pthread_init();
  return runtime->metadata.concurrency;
}

int pthread_setcancelstate(int state, int *oldstate) {
  mock_pthread_init();
  if (runtime->threads[runtime->get_tid()]->cancelable) {
    *oldstate = PTHREAD_CANCEL_ENABLE;
  } else {
    *oldstate = PTHREAD_CANCEL_DISABLE;
  }
  if (state == PTHREAD_CANCEL_ENABLE) {
    runtime->threads[runtime->get_tid()]->cancelable = true;
  } else {
    runtime->threads[runtime->get_tid()]->cancelable = false;
  }
  return 0;
}

int pthread_setcanceltype(int type, int *oldtype) {
  mock_pthread_init();
  return 0;
}

int pthread_yield() {
  mock_pthread_init();
  runtime->yield();
  return 0;
}

int pthread_mutexattr_init(pthread_mutexattr_t *attr) {
  mock_pthread_init();
  return 0;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t *attr) {
  mock_pthread_init();
  return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type) {
  mock_pthread_init();
  log(0, "warning: setting mutexattr type! unexplored code\n");
  log(0, "sizeof(mutexattr_t) == %d type == %d\n", sizeof(pthread_mutexattr_t), type);
  if (type == PTHREAD_MUTEX_NORMAL) {
    log(1, "Asking for normal mutex...\n");
  } else if (type == PTHREAD_MUTEX_NORMAL) {
    log(1, "Asking for default mutex...\n");
  } else if (type == PTHREAD_MUTEX_RECURSIVE) {
    log(1, "Asking for recursive mutex...\n");
  } else {
    log(0, "warning: unsupported mutexattr type\n");
  }
  *((int *) attr) = type;
  return 0;
}

int pthread_mutexattr_gettype(pthread_mutexattr_t *__restrict__ attr,
  int *__restrict__ type) {
  log(0, "getting the type...\n");
  *type = *((int *) attr);
  return 0;
}
