#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

pthread_cond_t cond;
pthread_mutex_t mutex;

void *start(void *arg) {
  pthread_t tid = pthread_self();
  printf("W000t! %d I'm gonna do something\n", tid);
  printf("Okay let's wait to be signaled\n");
  pthread_mutex_lock(&mutex);
  pthread_cond_wait(&cond, &mutex);
  pthread_mutex_unlock(&mutex);
  printf("After cond wait\n");
}

int main(int argc, char **argv) {
  pthread_t tid = pthread_self();
  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&cond, NULL);

  pthread_t thread;
  printf("%d is creating a thread...\n", thread);
  pthread_create(&thread, NULL, start, NULL);

  printf("W00t! Gonna signal the cond\n");
  pthread_mutex_lock(&mutex);
  pthread_cond_signal(&cond);
  pthread_mutex_unlock(&mutex);
  printf("After the signal...\n");

  pthread_join(thread, NULL);

  pthread_cond_destroy(&cond);
  pthread_mutex_init(&mutex, NULL);

  return 0;
}
