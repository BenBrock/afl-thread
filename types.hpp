#pragma once

#include <pthread.h>

static pthread_mutex_t default_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t default_cond = PTHREAD_COND_INITIALIZER;

extern "C" {
  typedef struct {
    int64_t lock;
    int type = PTHREAD_MUTEX_NORMAL;
  } semaphore;
  typedef struct {
    int8_t signaled = true;
    int8_t broadcast = false;
  } cond_t;
}
