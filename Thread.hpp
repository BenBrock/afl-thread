#pragma once

#include <sys/mman.h>
#include <stdexcept>
#include "types.hpp"
#include "globals.hpp"
#include "metadata.hpp"

void call_thread_fn(void *arg);

struct Thread {
  int tid;
  bool suspended = false;
  bool detached = false;
  bool dead = false;
  void *result = NULL;
  bool cancelable = true;
  cond_t *cond = NULL;
  ucontext_t context;

  Thread(const int tid) : tid(tid) {}

  Thread (const int tid, void *(funcp)(void *), void *arg) : tid(tid) {
    ucontext_t child_context;
    getcontext(&child_context);
    child_context.uc_link = 0;
    child_context.uc_stack.ss_sp = mmap(0, STACK_SIZE, PROT_READ|PROT_WRITE,
      MAP_PRIVATE|MAP_ANONYMOUS, 0, 0);
    child_context.uc_stack.ss_size = STACK_SIZE;
    child_context.uc_stack.ss_flags = 0;
    if (child_context.uc_stack.ss_sp == 0) {
      throw std::runtime_error("Mock Pthreads Error: OS cowardly refusing my \
        request for " + std::to_string(STACK_SIZE) + " bytes for tid " +
        std::to_string(tid) + "'s stack.");
    }
    call_thread_args *call_args = new call_thread_args(funcp, arg);
    makecontext(&child_context, reinterpret_cast <void (*)()> (call_thread_fn), 1, call_args);
    this->context = child_context;
  }
  ~Thread() {
    munmap(context.uc_stack.ss_sp, STACK_SIZE);
  }

  bool cond_waiting() {
    return cond != NULL;
  }

  // Wait on cond
  void cond_wait(cond_t *cond) {
    this->cond = cond;
  }

  void print() {
    printf("Thread %d\n", tid);
    printf("  Suspended? %s\n", (suspended) ? "true" : "false");
    printf("  detached? %s\n", (detached) ? "true" : "false");
    printf("  dead? %s\n", (dead) ? "true" : "false");
    printf("  result: %p\n", result);
    printf("  cond: %p\n", cond);
  }
};
