#pragma once

#include <unordered_map>
#include <vector>
#include <set>
#include <memory>
#include <sys/mman.h>

#include "Thread.hpp"
#include "metadata.hpp"
#include "logging.hpp"

struct ThreadRuntime {
  int thread_nonce = 0;
  int cur_tid = 0;
  int dec_idx = 0;
  std::unordered_map <int, std::shared_ptr <Thread>> threads;
  std::set <int> live_tids;

  std::vector <unsigned int> decisions;

  metadata_t metadata;

  ThreadRuntime() {
    cur_tid = thread_nonce++;
    threads[cur_tid] = std::shared_ptr <Thread> (new Thread(cur_tid));
    live_tids.insert(cur_tid);
  }

  int get_tid() {
    return cur_tid;
  }

  bool thread_exists(const int tid) {
    return threads.find(tid) != threads.end();
  }

  void *join(const int tid) {
    while (!threads[tid]->dead) {
      yield();
    }
    void *result = threads[tid]->result;
    threads.erase(threads.find(tid));
    live_tids.erase(tid);
    return result;
  }

  void im_dead(void *result) {
    threads[cur_tid]->dead = true;
    threads[cur_tid]->result = result;

    if (threads[cur_tid]->detached) {
      join(cur_tid);
    } else {
      live_tids.erase(cur_tid);
    }
    schedule();
  }

  void detach(const int tid) {
    if (thread_exists(tid)) {
      threads[tid]->detached = true;
    }
  }

  int launch(void *(*funcp) (void *), void *args) {
    int child_tid = thread_nonce++;
    threads[child_tid] = std::shared_ptr <Thread> (new Thread(child_tid, funcp, args));
    live_tids.insert(child_tid);

    yield();

    return child_tid;
  }

  void yield() {
    schedule();
  }

  void schedule() {
    /*
    log(2, "Making a scheduling decision among %d/%d threads\n", live_tids.size(),
      threads.size());
      */
    if (live_tids.size() == 0) {
      return;
    } else {
      int index;
      if (dec_idx < decisions.size()) {
        index = decisions[dec_idx++];
      } else {
        index = lrand48();
      }
      index %= live_tids.size();
      int tid = *std::next(live_tids.begin(), index);

      if (tid != cur_tid) {
        int old_tid = cur_tid;
        cur_tid = tid;
        swapcontext(&threads[old_tid]->context, &threads[cur_tid]->context);
        cur_tid = old_tid;
      }
    }
  }
};
