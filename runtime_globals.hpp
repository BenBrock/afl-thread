#pragma once

#include "ThreadRuntime.hpp"

bool mock_pthread_initialized = false;
ThreadRuntime *runtime;

void declare_dead(void *result);

void call_thread_fn(void *arg) {
  call_thread_args *a = (call_thread_args *) arg;
  void *result = a->funcp(a->args);
  delete a;
  declare_dead(result);
}

void declare_dead(void *result) {
  runtime->im_dead(result);
}
